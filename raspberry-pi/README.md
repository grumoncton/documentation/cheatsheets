# Configuration d'une Raspberry Pi

## Télécharger Raspbian Lite et installer sur une microSD

Il existe deux méthodes pour installer Raspbian sur une microSD :

1. Installer Raspbian avec le Raspberry Pi Imager
2. Télécharger Raspbian et l'installer manuellement

### Installation de Raspbian avec le Raspberry Pi Imager

Pour cela, il faut installer [Raspberry Pi Imager](https://www.raspberrypi.org/downloads/).

Une fois installé, il n'y a plus qu'à choisir _Raspbian Lite_ dans le menu et à choisir la microSD dans le menu du logiciel. Cliquez sur <kbd>Write</kbd>, et le tour est joué ! :smile:🦾

### Télécharger Raspbian Lite et installer manuellement

Il faut télécharger le zip de _Raspbian Lite_ (disponible [ici](https://www.raspberrypi.org/downloads/raspbian/)).

Une fois le zip téléchargé, il faut installer Raspbian sur la microSD. Pour cela, [Balena Etcher](https://www.balena.io/etcher/) est un bon logiciel à utiliser.

Une fois _Balena Etcher_ installé, sélectionnez le zip de _Raspbian_ que vous avez téléchargé et sélectionnez la microSD. Cliquez sur <kbd>Flash !</kbd>, et le tour est joué ! :relaxed:🦾

## Headless setup

[Source](https://www.raspberrypi.org/documentation/configuration/wireless/headless.md)

Dans la microSD, créez un fichier `ssh` et un fichier `wpa_supplicant.conf` :

```sh
touch ssh
touch wpa_supplicant.conf
```

Dans le fichier `wpa_supplicant.conf`, entrez le contenu suivant, et ajoutez les informations suivantes :

* Pays
* Nom du réseau Wi-Fi
* Mot de passe du Wi-Fi

```conf
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=<Insert country code here>

network={
 ssid="<Name of your WiFi>"
 psk="<Password for your WiFi>"
}
```

Il faut ensuite créer un user : [Lien vers la doc](https://www.raspberrypi.com/documentation/computers/configuration.html#configuring-a-user)

Voilà, votre Raspberry Pi est configurée pour que vous puissiez y accéder à distance \o/🦾

## SSH vers la Raspberry Pi

Branchez la Raspberry Pi avec la micro SD dedans et connectez-vous aux même réseau Wi-Fi que celui de votre Raspberry Pi.

Il vous faut ensuite déterminer l'adresse IP de votre Pi. Cela peut se faire en accédant au routeur du GRUM et en y cherchant les adresses IP des périphériques qui y sont connectés.

Dans Git Bash, entrez la commande suivante :

```sh
ssh pi@<adresse-ip>
```

Le mot de passe de connexion est `raspberry`.

## (Optionnel) Ajouter votre clé SSH dans la Pi

[Source](https://www.raspberrypi.org/documentation/remote-access/ssh/passwordless.md)

Si vous ne voulez pas entrer de mot de passe à chaque connexion, il est possible d'ajouter votre clé SSH dans la Pi avec la commande suivante :

```sh
ssh-copy-id pi@<adresse-ip>
```

## Changement du nom d'hôte

[Source](https://geek-university.com/raspberry-pi/change-raspberry-pis-hostname/)

Utilisez la commande suivante pour accéder au menu de configuration :

```sh
sudo raspi-config
```

Dans le menu, sélectionnez `System Options`, puis `Hostname`.
Vous pourrez changer le nom d'hôte de la Raspberry Pi.

## Git & ZSH

[Source](https://www.uberbuilder.com/oh-my-zsh-on-raspberry-pi/)

Installez `git` et `zsh` :

```sh
sudo apt update
sudo apt upgrade
sudo apt install git zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

```sh
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:=~/.oh-my-zsh/custom}/plugins/zsh-completions
```

## Options

### Static IP address

[Source](https://raspberrypi.stackexchange.com/questions/37920/how-do-i-set-up-networking-wifi-static-ip-address)

### Remote Jupyter Notebook

[Source](https://www.techcoil.com/blog/how-to-setup-jupyter-notebook-on-raspberry-pi-3-with-raspbian-stretch-lite-supervisor-and-virtualenv-to-run-python-3-codes/)

### Interface Graphique PIXEL

[Source](https://dadarevue.com/ajouter-gui-raspbian-lite/)


