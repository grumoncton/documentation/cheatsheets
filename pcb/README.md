<!--
vim: spelllang=fr
-->

# Cheatsheet PCB

## TL;DR
* Vias: 30mil outer diameter et 14mil drill
* Surface mount package: 1608 métrique (0603 impérial) or bigger

## Leçons
Voici quelques leçons que nous avons appris the hard way.
1. Pour les composantes surface mount, utiliser des packages plus gros que 0603
	 métrique et probablement plus gros que 1005 métrique.
1. Pour les vias, utiliser une taille standard. Probablement outer diameter de
	 30mil et drill de 14mil.

## Tailles des composantes surface mount ([source](http://www.resistorguide.com/resistor-sizes-and-packages/)):
| Metric | Imperial | Length | Width  | Height | Power  |
| ------ | -------- | ------ | ------ | ------ | ------ |
| 0603   | 0201     | 0.6mm  | 0.3mm  | 0.25mm | 0.05W  |
| 1005   | 0402     | 1.0mm  | 0.5mm  | 0.35mm | 0.062W |
| 1608   | 0603     | 1.55mm | 0.85mm | 0.45mm | 0.10W  |
| 2012   | 0805     | 2.0mm  | 1.2mm  | 0.45mm | 0.125W |
| 3216   | 1206     | 3.2mm  | 1.6mm  | 0.55mm | 0.25W  |
| 3225   | 1210     | 3.2mm  | 2.5mm  | 0.55mm | 0.5W   |
| 3246   | 1218     | 3.2mm  | 4.6mm  | 0.55mm | 1W     |
| 5025   | 2010     | 5.0mm  | 2.5mm  | 0.6mm  | 0.75W  |
| 6332   | 2512     | 6.3mm  | 3.2mm  | 0.6mm  | 1W     |

![Chip Resistor Size Comarison](./chip-resistor-size-comparison.png)
