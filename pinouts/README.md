<!--
vim: spelllang=fr
-->

# Pinouts pour les composantes électriques qu'on utilise souvent

## Raspberry Pi

![Raspberry Pi 3 Model B](./raspberry-pi-3-B.jpg)

## Teensy

### Teensy 4.0

[Official docs](https://www.pjrc.com/store/teensy40.html)
[PlatformIO](https://docs.platformio.org/en/latest/boards/teensy/teensy40.html)
![Teensy 4.0](./teensy40.jpg)

## STMicroelectronics

### NUCLEO-F303K8

[Mbed | NUCLEO-F303K8](https://os.mbed.com/platforms/ST-Nucleo-F303K8/)
![NUCLEO-F303K8](./nucleo-f303k8.png)

### NUCLEO-F401RE

[Mbed | NUCLEO-F401RE](https://os.mbed.com/platforms/ST-Nucleo-F401RE/)
![NUCLEO-F401RE arduino header](./nucleo-f401re-arduino.png)
![NUCLEO-F401RE morpho header](./nucleo-f401re-morpho.png)

## AMT10 Rotary Encoder

[Digikey](https://www.digikey.com/product-detail/en/cui-inc/AMT102-V/102-1307-ND/827015)
![AMT10 pinout](./AMT10.png)

## Capteurs *Polulu*

### IMU

[Site web](https://www.pololu.com/product/2738)
![Polulu IMU pinout](./MinIMU-9-v5.jpg)

### ToF

[Site web](https://www.pololu.com/product/2490)
![Polulu ToF pinout](./Polulu-VL53L0X.jpg)

## Communication UART

### FTDI FT232RL USB to TTL Adapter

[Information](https://stak.com/USB_to_TTL_Serial_Adapter__FTDI_FT232RL_chipset__3.3V_and_5V_compatible)
![FTDI FT232RL USB to TTL Adapter pinout](serial-ftdi-ft232rl-yp05.png)

Connexions :

| Adapteur | Raspberry Pi    |
| -------- | --------------- |
| RX       | UART0_TXD ( 8)  |
| TX       | UART0_RXD ( 10) |
| GND      | GND             |
