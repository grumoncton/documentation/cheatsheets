# Cours en ligne

> Liste de cours en ligne

Rédigé par Damien LaRocque, 2018-12-22

- [Cours en ligne](#cours-en-ligne)
  - [Web](#web)
  - [Mobile](#mobile)
  - [Kit Arduino (pour les GELE seulement)](#kit-arduino-pour-les-gele-seulement)
  - [Autres](#autres)

## Web

- **OpenClassrooms** (<https://openclassrooms.com/fr/>) est un site français de cours en ligne.
  - Ils offraient auparavant des cours en texte, mais maintenant, ils offrent des cours en vidéo. Cependant, la majorité des cours en format vidéo nécessitent un abonnement _Premium_. Du coup, je vous suggère de lire les cours en format texte, plus complets et meilleurs.
  - Python : <https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python>

- **DataCamp** est, selon moi, le meilleur site de cours en ligne pour tout ce qui touche à l’analyse de données.
  - DataCamp a limité certains chapitres de ses cours pour une offre payante. Malgré tout, l’offre gratuite de DataCamp est très complète et permet déjà de bien programmer.
  - Je vous recommande les cours de Python, de Git et de Shell. Les cours de Git et Shell sont complètement gratuits.
  - <https://www.datacamp.com/>

- **Udacity** est un autre bon site de cours en ligne offerts par différentes compagnies, telles que Google, Facebook ou encore Amazon.
  - Ils ont récemment créé une offre payante sous forme de _nanodegrees_. Par contre, il est possible de suivre les cours de ces _nanodegrees_ gratuitement, en sélectionnant _FreeCourses_ dans les _Program Details_.
  - Leurs cours sont offerts en vidéo.
  - Je n’ai pas grand cours à vous recommander, mais n’hésitez pas à suivre votre curiosité.
  - <https://www.udacity.com/>

## Mobile

Si vous avez du temps à perdre sur votre téléphone, quelques applications intéressantes :

- **SoloLearn** : <https://www.sololearn.com/>
  - SoloLearn a plusieurs applications : Une application générale et une application pour chaque langage. Je vous recommande de télécharger l’application générale, car elle contient les cours des autres applications et plus encore.
  - Les cours de SoloLearn ne sont pas très complets, mais ils ont l’avantage d’avoir des quiz où vous pourrez appliquer ce que vous avez appris. Je vous recommande de l’utiliser avec d’autres cours.

- **DataCamp** a aussi une bonne application mobile tant sur Apple qu’Android :
  - <https://www.datacamp.com/mobile>.

- **Udacity** a une application mobile. Elle n’est pas très ergonomique, mais elle permet d’écouter les vidéos des cours en ligne.
  - <https://www.udacity.com/mobile>

## Kit Arduino (pour les GELE seulement)

Si vous êtes en génie électrique et que vous avez un peu de budget, nous vous recommandons de vous acheter un kit Arduino. Un kit d’une cinquantaine de dollars suffit pour expérimenter. Ce faisant, un bon cours à suivre sur Arduino est sur OpenClassrooms. <https://openclassrooms.com/fr/courses/2778161-programmez-vos-premiers-montages-avec-arduino>

## Autres

Enfin, Marcel et moi essaierons de vous faire des exercices où vous devrez commenter des sections de code. Ça vous fera pratiquer la lecture de documentation, une des compétences les plus importantes quand on apprend à programmer.

**TL;DR** Voici des liens de cours en ligne, tant sur ordi que sur téléphone. Choisissez ceux que vous voulez. DataCamp est bien pour apprendre Git et Shell (ce qu'on utilise dans Git Bash). Sinon, OpenClassrooms semble être un bon choix pour un cours complet de Python. Pour ceux en GELE, c'est une bonne idée d'investir dans une kit Arduino. Pas besoin de beaucoup dépenser pour avoir un kit convenable. Nous essaierons de vous faire lire de la documentation. Mais n'oubliez pas que c'est en programmant qu'on devient programmeur.
