# Cheatsheets

> Ah, j'ai oublié comment faire cela.

Cheatsheets des différents outils utilisés au GRUM. Ce repo est à mettre à jour au fur et à mesure de l'adoption de nouveaux outils.

## PINOUTS

[Voir pinouts](pinouts/README.md)

## PCB

[PCB](pcb/README.md)

## Configuration de Raspberry Pi

[Configuration d'une Raspberry Pi](raspberry-pi/README.md)






## Cours en Ligne

[Liste de cours en ligne](CoursEnLigne.md)


